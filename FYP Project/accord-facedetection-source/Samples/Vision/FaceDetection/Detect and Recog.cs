﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Accord.Imaging.Filters;
using Accord.Vision.Detection;
using Accord.Vision.Detection.Cascades;
using System.Diagnostics;
using System.Collections;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;

using System.IO;
using System.Collections.Generic;

namespace FaceDetection
{
    public partial class DetectAndRecognize : Form
    {
        Bitmap picture;// = FaceDetection.Properties.Resources.judybats;
        Rectangle[] objects;
        RectanglesMarker marker;
        HaarObjectDetector detector;
        int recognitions;
        
        
        //Recognize
         Image<Bgr, Byte> currentFrame, currentFrame1;
        Capture grabber;
        Emgu.CV.HaarCascade face;
        Emgu.CV.HaarCascade eye;
        MCvFont font = new MCvFont(FONT.CV_FONT_HERSHEY_TRIPLEX, 0.5d, 0.5d);
        Image<Gray, byte> result, TrainedFace = null;
        Image<Gray, byte> gray = null;
        List<Image<Gray, byte>> trainingImages = new List<Image<Gray, byte>>();
        List<string> labels = new List<string>();
        List<string> NamePersons = new List<string>();
        List<int> ids = new List<int>();

        int ContTrain, NumLabels, t;
        string name, names = null;

        public DetectAndRecognize()
        {
            InitializeComponent();

            
            //  pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            cbMode.DataSource = Enum.GetValues(typeof(ObjectDetectorSearchMode));
            cbScaling.DataSource = Enum.GetValues(typeof(ObjectDetectorScalingMode));

            cbMode.SelectedItem = ObjectDetectorSearchMode.NoOverlap;
            cbScaling.SelectedItem = ObjectDetectorScalingMode.SmallerToGreater;

       //     toolStripStatusLabel1.Text = "Please select the detector options and click Detect to begin.";

            Accord.Vision.Detection.HaarCascade cascade = new FaceHaarCascade();
            detector = new HaarObjectDetector(cascade, 30);

            recognitions = 0;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            detector.SearchMode = (ObjectDetectorSearchMode)cbMode.SelectedValue;
            detector.ScalingMode = (ObjectDetectorScalingMode)cbScaling.SelectedValue;
            detector.ScalingFactor = 1.5f;
            detector.UseParallelProcessing = cbParallel.Checked;

            Stopwatch sw = Stopwatch.StartNew();


            // Process frame to detect objects
            objects = detector.ProcessFrame(currentFrame.ToBitmap());

            /*    for(int i=0;i<objects.Length;i++)
                {
                   
            Bitmap nb = picture.Clone(objects[i],picture.PixelFormat);
                    String path="crops\\"+i.ToString()+".jpg";
            nb.Save(path);
                }
            */

            sw.Stop();


            if (objects.Length > 0)
            {
                marker = new RectanglesMarker(objects, Color.Red);


                pictureBox1.Image = marker.Apply(currentFrame.ToBitmap());


            }

            
           // toolStripStatusLabel1.Text = string.Format("Completed detection of {0} objects in {1}.",
             //   objects.Length, sw.Elapsed);
            /////////////////////////////////Load For Recognition
            string Labelsinfo = File.ReadAllText(Application.StartupPath + "/TrainedFaces/TrainedLabels.txt");
            string[] Labels = Labelsinfo.Split('%');
            NumLabels = Convert.ToInt16(Labels[0]);
            ContTrain = NumLabels;
            string LoadFaces;

            for (int tf = 1; tf < NumLabels + 1; tf++)
            {
                LoadFaces = "face" + tf + ".bmp";
                trainingImages.Add(new Image<Gray, byte>(Application.StartupPath + "/TrainedFaces/" + LoadFaces));
                ids.Add(labels.Count);
                labels.Add(Labels[tf]);
             
            }
            




        }
        void FrameGrabber()
        {
            //label3.Text = "0";
            //label4.Text = "";
            NamePersons.Add("");


            //Get the current frame form capture device
            //  currentFrame = grabber.QueryFrame().Resize(320, 240, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);

            //Convert it to Grayscale
            gray = currentFrame.Convert<Gray, Byte>();

            //Face Detector

           

            //Action for each element detected
           // foreach (MCvAvgComp f in facesDetected[0])
            MCvAvgComp f;
            currentFrame1 = new Image<Bgr, Byte>(picture);
            foreach(Rectangle fa in objects)
            {
                f.rect = fa;
                t = t + 1;


                double x = Convert.ToDouble(picture.Width) / Convert.ToDouble(currentFrame.Width);
                double y = Convert.ToDouble(picture.Height) / Convert.ToDouble(currentFrame.Height);


                double t1 = Convert.ToDouble(f.rect.X);
                double t2 = Convert.ToDouble(f.rect.Y);

                t1 *= x;
                t2 *= y;


                double s1 = Convert.ToDouble(f.rect.Width);
                double s2 = Convert.ToDouble(f.rect.Height);

                f.rect.X = (int)t1;
                f.rect.Y = (int)t2;


                s1 *= x;
                s2 *= y;

                f.rect.Width = (int)s1;
                f.rect.Height = (int)s2;
                      



                result = currentFrame1.Copy(f.rect).Convert<Gray, byte>().Resize(100, 100, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);
                //draw the face detected in the 0th (gray) channel with blue color
                currentFrame.Draw(fa, new Bgr(Color.Red), 2);


                if (trainingImages.ToArray().Length != 0)
                {
                    //TermCriteria for face recognition with numbers of trained images like maxIteration
                    MCvTermCriteria termCrit = new MCvTermCriteria(ContTrain, 0.001);

                    //Eigen face recognizer
                    EigenObjectRecognizer recognizer = new EigenObjectRecognizer(
                       trainingImages.ToArray(),
                       labels.ToArray(),
                       3000,
                       ref termCrit);

                    name = recognizer.Recognize(result);

                    if (name != String.Empty)
                    { recognitions++; }
                  //  label3.Text = recognizer.EigenDistanceThreshold.ToString();
                    
                        //Draw the label for each face detected and recognized
                    currentFrame.Draw(name, ref font, new Point(fa.X - 2, fa.Y - 2), new Bgr(Color.LightGreen));

                }

                NamePersons[t - 1] = name;
                NamePersons.Add("");


                //Set the number of faces detected on the scene
              //////////////////////////  label3.Text = facesDetected[0].Length.ToString();

              

            }
            t = 0;

            //Names concatenation of persons recognized
          
            //Show the faces procesed and recognized
            ////////////////////imageBoxFrameGrabber.Image = currentFrame.Resize(600, 500, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);
            ////////////////////label4.Text = names;
            pictureBox1.Image = currentFrame.Resize(pictureBox1.Width,pictureBox1.Height, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC).ToBitmap();

            for (int i = 0; i < objects.Length; i++)
            {
                names = names + NamePersons[i] + ", ";
            }
            
            
            names = "";

            label6.Text = objects.Length.ToString();


           
            //Clear the list(vector) of names
            NamePersons.Clear();

        }


        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {

            if (e.Button == MouseButtons.Left)
            {



                Point position = new Point(e.X, e.Y);//pictureBox1.PointToClient(Cursor.Position);

                if (objects != null)
                {


                    for (int i = 0; i < objects.Length; i++)
                    {

                        // Rectangle r=pictureBox1.RectangleToClient(objects[i]);
                        //MessageBox.Show(position.ToString()+" - "+ objects[i].Left.ToString() + " - " + objects[i].Right.ToString() + " - " + objects[i].Bottom.ToString() + " - " + objects[i].Top.ToString());
                        //if (position.X >= objects[i].Left && position.X <= objects[i].Right && position.Y >= objects[i].Top && position.Y <= objects[i].Bottom)
                        if (objects[i].Contains(position))
                        {

                            //MessageBox.Show(objects[i].Top.ToString()+"   "+position.ToString());

                             
                            Info form = new Info(objects[i], picture);

                            form.Show();


                        }
                    }
                }
            }
        }

        private void Training_Load(object sender, EventArgs e)
        {

        }

        private void imageBox1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Open Image";
                dlg.Filter = "All Files (*.*)|*.*";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    //PictureBox PictureBox1 = new PictureBox();

                    // Create a new Bitmap object from the picture file on disk,
                    // and assign that to the PictureBox.Image property
                    //                    currentFrame = new Image<Bgr, Byte>(picture).Resize(300, 300, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);

                    picture = new Bitmap(dlg.FileName);

                    double x = picture.Width;
                    double y = picture.Height;
                    double ratio;
                    if (x > y)
                    { ratio = x / y; }
                    else
                    {
                        ratio = y / x;
                    }

                    while (x > pictureBox1.Width || y > pictureBox1.Height)
                    {

                        if (x > y)
                        {
                            x -= ratio;
                            y--;
                        }
                        else
                        {
                            y -= ratio;
                            x--;
                        }

                    }



                    currentFrame = new Image<Bgr, Byte>(picture).Resize((int)x, (int)y, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);

                    pictureBox1.Image = currentFrame.ToBitmap();


                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MainController Check = new MainController();
           Check.Show();
           Hide();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            double numberOfFaces = Convert.ToDouble(textBox1.Text);
            double wrongDetection = Convert.ToDouble(textBox2.Text);
            double wrongRecognitions = Convert.ToDouble(textBox3.Text);
            double aAcc = (objects.Length - wrongDetection)/numberOfFaces;
             aAcc *= 100;
           
            double rAcc=((recognitions-wrongRecognitions) / numberOfFaces) * 100;
            label10.Text = "No Of Correct Faces Detected : " + (objects.Length - wrongDetection).ToString();
            label11.Text = "No Of Correct Recognitions : " + (recognitions - wrongRecognitions).ToString();
            label12.Text = "Detection Accuracy : " + aAcc.ToString() + " %";
            label13.Text = "Recognition Accuracy : " + rAcc.ToString() +" %";
            
        }


      /*  void FrameGrabber_Standard(Classifier_Train Eigen_Recog)
        {
            //Get the current frame form capture device   
            currentFrame1 = new Image<Bgr, Byte>(picture);

            //Convert it to Grayscale
            if (currentFrame != null)
            {
               // gray_frame = currentFrame.Convert<Gray, Byte>();

                //Face Detector
               
                //Action for each element detected
                for (int i = 0; i < objects.Length; i++)// (Rectangle face_found in facesDetected)
                {
                    //This will focus in on the face from the haar results its not perfect but it will remove a majoriy
                    //of the background noise


                    Rectangle f = objects[i];
                    double x = Convert.ToDouble(picture.Width) / Convert.ToDouble(currentFrame.Width);
                    double y = Convert.ToDouble(picture.Height) / Convert.ToDouble(currentFrame.Height);


                    double t1 = Convert.ToDouble(objects[i].X);
                    double t2 = Convert.ToDouble(objects[i].Y);

                    t1 *= x;
                    t2 *= y;


                    double s1 = Convert.ToDouble(objects[i].Width);
                    double s2 = Convert.ToDouble(objects[i].Height);

                    f.X = (int)t1;
                    f.Y = (int)t2;


                    s1 *= x;
                    s2 *= y;

                    f.Width = (int)s1;
                    f.Height = (int)s2;
               

                    result = currentFrame1.Copy(f).Convert<Gray, byte>().Resize(100, 100, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);
                    result._EqualizeHist();
                    //draw the face detected in the 0th (gray) channel with blue color
                    currentFrame.Draw(objects[i], new Bgr(Color.Red), 2);

                   
                        string name = Eigen_Recog.Recognise(result);
                        int match_value = (int)Eigen_Recog.Get_Eigen_Distance;
                         //Draw the label for each face detected and recognized
                        currentFrame.Draw(name + " ", ref font, new Point(f.X - 2, f.Y - 2), new Bgr(Color.LightGreen));
                        
                    
                }
                //Show the faces procesed and recognized
                pictureBox1.Image = currentFrame.Resize(pictureBox1.Width, pictureBox1.Height, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC).ToBitmap();

            }
        }*/


        
        private void button5_Click(object sender, EventArgs e)
        {
            detector.SearchMode = (ObjectDetectorSearchMode)cbMode.SelectedValue;
            detector.ScalingMode = (ObjectDetectorScalingMode)cbScaling.SelectedValue;
            detector.ScalingFactor = 1.5f;
            detector.UseParallelProcessing = cbParallel.Checked;

            Stopwatch sw = Stopwatch.StartNew();


            objects = detector.ProcessFrame(currentFrame.ToBitmap());


            sw.Stop();


            if (objects.Length > 0)
            {
                marker = new RectanglesMarker(objects, Color.Red);


                pictureBox1.Image = marker.Apply(currentFrame.ToBitmap());


            }


             
            
          

        }

        private void button5_Click_1(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            FrameGrabber();
        }

    }
}
