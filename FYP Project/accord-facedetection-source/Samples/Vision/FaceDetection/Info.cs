﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using System.IO;
using System.Diagnostics;

using System.Drawing.Imaging;
using System.Xml;
using System.Threading;
namespace FaceDetection
{
    public partial class Info : Form
    {
        Rectangle objects;
        Bitmap picture;
        XmlDocument docu = new XmlDocument();
        
        Image<Gray, byte> TrainedFace = null;
        public Info()
        {
            
            InitializeComponent();
        }
        public Info(Rectangle r, Bitmap pic)
        {
            objects=r;
            picture=pic;
            InitializeComponent();
        }

        private bool save_training_data(Image face_data,string name)
        {
            try
            {
                Random rand = new Random();
                bool file_create = true;
                string facename = "face_" + name + "_" + rand.Next().ToString() + ".jpg";
                while (file_create)
                {

                    if (!File.Exists(Application.StartupPath + "/lbpTrained/" + facename))
                    {
                        file_create = false;
                    }
                    else
                    {
                        facename = "face_" + name + "_" + rand.Next().ToString() + ".jpg";
                    }
                }


                if (Directory.Exists(Application.StartupPath + "/lbpTrained/"))
                {
                    face_data.Save(Application.StartupPath + "/lbpTrained/" + facename, ImageFormat.Jpeg);
                }
                else
                {
                    Directory.CreateDirectory(Application.StartupPath + "/TrainedFaces/");
                    face_data.Save(Application.StartupPath + "/lbpTrained/" + facename, ImageFormat.Jpeg);
                }
                if (File.Exists(Application.StartupPath + "/lbpTrained/TrainedRolls.xml"))
                {
                    //File.AppendAllText(Application.StartupPath + "/TrainedFaces/TrainedLabels.txt", NAME_PERSON.Text + "\n\r");
                    bool loading = true;
                    while (loading)
                    {
                        try
                        {
                            docu.Load(Application.StartupPath + "/lbpTrained/TrainedRolls.xml");
                            loading = false;
                        }
                        catch
                        {
                            docu = null;
                            docu = new XmlDocument();
                            Thread.Sleep(10);
                        }
                    }

                    //Get the root element
                    XmlElement root = docu.DocumentElement;

                    XmlElement face_D = docu.CreateElement("FACE");
                    XmlElement name_D = docu.CreateElement("NAME");
                    XmlElement file_D = docu.CreateElement("FILE");

                    //Add the values for each nodes
                    //name.Value = textBoxName.Text;
                    //age.InnerText = textBoxAge.Text;
                    //gender.InnerText = textBoxGender.Text;
                    name_D.InnerText = name;
                    file_D.InnerText = facename;

                    //Construct the Person element
                    //person.Attributes.Append(name);
                    face_D.AppendChild(name_D);
                    face_D.AppendChild(file_D);

                    //Add the New person element to the end of the root element
                    root.AppendChild(face_D);

                    //Save the document
                    docu.Save(Application.StartupPath + "/lbpTrained/TrainedRolls.xml");
                    //XmlElement child_element = docu.CreateElement("FACE");
                    //docu.AppendChild(child_element);
                    //docu.Save("TrainedLabels.xml");
                }
                else
                {
                    FileStream FS_Face = File.OpenWrite(Application.StartupPath + "/lbpTrained/TrainedRolls.xml");
                    using (XmlWriter writer = XmlWriter.Create(FS_Face))
                    {
                        writer.WriteStartDocument();
                        writer.WriteStartElement("Faces_For_Training");

                        writer.WriteStartElement("FACE");
                        writer.WriteElementString("NAME", name);
                        writer.WriteElementString("FILE", facename);
                        writer.WriteEndElement();

                        writer.WriteEndElement();
                        writer.WriteEndDocument();
                    }
                    FS_Face.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            string[] fields;
            string s;
            char delimeter = '%';
            
            if (File.Exists(Application.StartupPath + "/TrainedFaces/Names.txt"))
            {
                 
                File.AppendAllText(Application.StartupPath + "/TrainedFaces/Names.txt", roll.Text + "%");
 }
            else
            {


                File.WriteAllText(Application.StartupPath + "/TrainedFaces/Names.txt",roll.Text + "%");

            }







          





            s = File.ReadAllText(Application.StartupPath + "/TrainedFaces/Names.txt");
                
            fields = s.Split(delimeter);

            int l = fields.Length;
             

            TrainedFace = new Image<Gray, Byte>(picture.Clone(objects, picture.PixelFormat)).Resize(100, 100, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);
               

            TrainedFace.Save(Application.StartupPath + "/TrainedFaces/face" + (l-1) + ".bmp");

            File.WriteAllText(Application.StartupPath + "/TrainedFaces/TrainedLabels.txt", l-1 + "%");

            for (int k = 0; k < fields.Length-1; k++ )
            {
                File.AppendAllText(Application.StartupPath + "/TrainedFaces/TrainedLabels.txt", fields[k]+"%");
            
            
            }

            save_training_data(TrainedFace.ToBitmap(), roll.Text);
            

             this.Close();
        
             }

        private void label1_Click(object sender, EventArgs e)
        {

        }

    

   
    }
}
