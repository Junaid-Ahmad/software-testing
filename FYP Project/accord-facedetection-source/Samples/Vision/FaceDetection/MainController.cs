﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FaceDetection
{
    public partial class MainController : Form
    {
        public MainController()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DetectAndRecognize Check = new DetectAndRecognize();
            Check.Show();
            Hide();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void MainController_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            CollectTrainingImages Check = new CollectTrainingImages();
            Check.Show();
            Hide();
        }
    }
}
