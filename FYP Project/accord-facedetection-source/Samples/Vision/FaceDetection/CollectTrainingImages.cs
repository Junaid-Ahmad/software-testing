﻿

using System;
using System.Drawing;
using System.Windows.Forms;
using Accord.Imaging.Filters;
using Accord.Vision.Detection;
using Accord.Vision.Detection.Cascades;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
namespace FaceDetection
{
    public partial class CollectTrainingImages : Form
    {
       Bitmap picture,picture1;
        Rectangle[] objects;
        RectanglesMarker marker;
        HaarObjectDetector detector;
        Image<Bgr, Byte> currentFrame;
        Emgu.CV.HaarCascade face;


        public CollectTrainingImages()
        {
            InitializeComponent();
           // Bitmap newImage = resizeImage(picture, pictureBox1.Size);

           // Bitmap clone = new Bitmap(newImage.Width, newImage.Height, System.Drawing.Imaging.PixelFormat.);
           
          //  pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            cbMode.DataSource = Enum.GetValues(typeof(ObjectDetectorSearchMode));
            cbScaling.DataSource = Enum.GetValues(typeof(ObjectDetectorScalingMode));

            cbMode.SelectedItem = ObjectDetectorSearchMode.NoOverlap;
            cbScaling.SelectedItem = ObjectDetectorScalingMode.SmallerToGreater;

            toolStripStatusLabel1.Text = "Please select the detector options and click Detect to begin.";

            Accord.Vision.Detection.HaarCascade cascade = new FaceHaarCascade();
            detector = new HaarObjectDetector(cascade, 30);
        }
        public static Bitmap resizeImage(Image imgToResize, Size size)
        {
            return (new Bitmap(imgToResize, size));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            detector.SearchMode = (ObjectDetectorSearchMode)cbMode.SelectedValue;
            detector.ScalingMode = (ObjectDetectorScalingMode)cbScaling.SelectedValue;
            detector.ScalingFactor = 1.5f;
            detector.UseParallelProcessing = cbParallel.Checked;

            Stopwatch sw = Stopwatch.StartNew();
            //Bitmap newImage = resizeImage(picture, pictureBox1.Size);

          //  Bitmap clone = new Bitmap(newImage.Width, newImage.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
       

            // Process frame to detect objects
             objects = detector.ProcessFrame(currentFrame.ToBitmap());
            /*    for(int i=0;i<objects.Length;i++)
                {
                   
            Bitmap nb = picture.Clone(objects[i],picture.PixelFormat);
                    String path="crops\\"+i.ToString()+".jpg";
            nb.Save(path);
                }
            */

            sw.Stop();
            

                if (objects.Length > 0)
                {
                    marker = new RectanglesMarker(objects, Color.Red);


                    pictureBox1.Image = marker.Apply(currentFrame.ToBitmap());


                }


               

                    toolStripStatusLabel1.Text = string.Format("Completed detection of {0} objects in {1}.",
                        objects.Length, sw.Elapsed);

        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            
            if (e.Button == MouseButtons.Left)
            {
                 
                

                Point position = new Point(e.X, e.Y);//pictureBox1.PointToClient(Cursor.Position);
              
                if(objects!=null)
                {
                
                    
                for (int i = 0; i < objects.Length; i++)
                {

                   // Rectangle r=pictureBox1.RectangleToClient(objects[i]);
                    //MessageBox.Show(position.ToString()+" - "+ objects[i].Left.ToString() + " - " + objects[i].Right.ToString() + " - " + objects[i].Bottom.ToString() + " - " + objects[i].Top.ToString());
                    //if (position.X >= objects[i].Left && position.X <= objects[i].Right && position.Y >= objects[i].Top && position.Y <= objects[i].Bottom)
                    if (objects[i].Contains(position))
                    {


                         

                        
                        double x = Convert.ToDouble(picture.Width) /Convert.ToDouble(currentFrame.Width);
                        double y = Convert.ToDouble(picture.Height) / Convert.ToDouble(currentFrame.Height);


                        double t1 = Convert.ToDouble(objects[i].X);
                        double t2 = Convert.ToDouble(objects[i].Y);

                        t1 *= x;
                        t2 *= y;

                        
                        double s1=Convert.ToDouble(objects[i].Width);
                        double s2=Convert.ToDouble(objects[i].Height);

                        objects[i].X = (int)t1;
                        objects[i].Y = (int)t2;

                        
                        s1*=x;
                        s2*=y;

                        objects[i].Width = (int)s1;
                        objects[i].Height = (int)s2;
                        Info form = new Info(objects[i],picture);
                    
                        form.Show();
                  
                        // 3264 1840->  1049 712        1059 597->    341 230
                    }
                }
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {

            MainController Check = new MainController();
            Check.Show();
            Hide();
             
        }

        private void cbMode_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void cbScaling_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {

        }

        private void cbParallel_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
           
        }

        private void button4_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Open Image";
                dlg.Filter = "All Files (*.*)|*.*";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    //PictureBox PictureBox1 = new PictureBox();

                    // Create a new Bitmap object from the picture file on disk,
                    // and assign that to the PictureBox.Image property
//                    currentFrame = new Image<Bgr, Byte>(picture).Resize(300, 300, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);

                    picture = new Bitmap(dlg.FileName);
                    picture1 = picture;
                    double x = picture.Width;
                    double y = picture.Height;
                    double ratio;
                    if (x > y)
                    { ratio = x / y; }
                    else {
                        ratio = y / x;
                    }

                    while (x > pictureBox1.Width || y > pictureBox1.Height)
                    {

                        if (x > y)
                        { x -= ratio;
                        y--;
                        }
                        else
                        {
                            y -= ratio;
                            x--;
                        }
                    
                    }

                    
                    
                    currentFrame = new Image<Bgr, Byte>(picture).Resize((int)x,(int)y, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);

                    pictureBox1.Image = currentFrame.ToBitmap();

                }
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
           // RecForm Check = new RecForm();
           // Check.Show();
            //Hide();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Image<Gray, byte> gray = null;
            gray = currentFrame.Convert<Gray, Byte>();

            face = new Emgu.CV.HaarCascade("haarcascade_frontalface_default.xml");
       
            MCvAvgComp[][] facesDetected = gray.DetectHaarCascade(
        face,
        1.2,
        10,
        Emgu.CV.CvEnum.HAAR_DETECTION_TYPE.DO_CANNY_PRUNING,
        new Size(20, 20));


            foreach (MCvAvgComp f in facesDetected[0])
            {
                currentFrame.Draw(f.rect, new Bgr(Color.Red), 2);
            }

            pictureBox1.Image = currentFrame.ToBitmap();


        }
        private void button6_Click(object sender, EventArgs e)
        {
        }



    }
}
